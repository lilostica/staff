$(document).ready(function () {
    let hideORShowState = false;
    $('#test').on('click',function () {
        if (hideORShowState)
        {
            $('.to-hide').attr('hidden',true);
            hideORShowState = false;
            sessionStorage.removeItem('open');
        }else {
            $('.to-hide').attr('hidden',false);
            console.log('tut pistt');
            sessionStorage.setItem('open',true);

            hideORShowState = true;
        }
    });

    let hideIsOpen = function () {
        if (sessionStorage.getItem('open'))
        {
            $('.to-hide').attr('hidden',false);
        }
    };
    hideIsOpen();

    const startTime = function () {
        $('.start-time').on('click',function () {

            let id = $(this).attr('data-user-id');
            let day = $(this).attr('data-day-id');
            let time = timeConstructor();
            let hourMinute = hourMinuteConstructor();
            let currentDate = $(this).attr('data-day')+'T'+time;
            console.log(new Date().getMonth());
            $.ajax({
                url: 'http://localhost:8000/time/start/or/stop',
                method: 'POST',
                data: {'time_state':'start','date' : currentDate,'user_id': id,'time': time}
            }).done(function (response) {
                $('#start-time-'+id+'-'+day).css('display','none');
                $('#stop-time-'+id+'-'+day).css('display','');
                $(`#time-block-${id}-${day}`)
                    .append(`<div class="row row-cols-2 stop-${id}-${day}"><div class="col">${hourMinute}</div></div>`);
            })
                .fail(function (response) {
                    console.log('FAIL RESPONSE =>', response);
                });
        });

        $('.stop-time').on('click',function () {

            let id = $(this).attr('data-user-id');
            let day = $(this).attr('data-day-id');
            let time = timeConstructor();
            let hourMinute = hourMinuteConstructor();
            let currentDate = $(this).attr('data-day')+'T'+time;
            let date = $(this).attr('data-day');
            $.ajax({
                url: 'http://localhost:8000/time/start/or/stop',
                method: 'POST',
                data: {'time_state':'stop','date' : currentDate,'user_id': id,'time': time}
            }).done(function (response) {
                $('#start-time-'+id+'-'+day).css('display','');
                $('#stop-time-'+id+'-'+day).css('display','none');
                if ($(`.stop-${id}-${day}:last`).is($(`.stop-${id}-${day}:last`)))
                {
                        $(`.stop-${id}-${day}:last`).append(`<div class="col">${hourMinute}</div>`);
                }else
                    {
                        $(`.stop-${id}-${day}:last`).append(`<div class="col">${hourMinute}</div>`);
                    }
                $.ajax({
                    url:`http://localhost:8000/time/total/${id}/${date}`,
                    method: 'POST',
                    success: function (total_time) {
                        total = total_time.substr(1,5);
                        $(`.total-time-${id}-${day}:last`).text(`${total}`);
                    }
                });
            })
                .fail(function (response) {
                    console.log('FAIL RESPONSE =>', response);
                });
        });
    };
    startTime();

    const timeConstructor = function () {
        let date = new Date();
        let hour = date.getHours();
        let minutes = date.getMinutes();
        let seconds = date.getSeconds();
        if(hour < 10)
        {
            hour = '0'+hour;
        }
        if (minutes <10)
        {
            minutes = '0'+minutes;
        }
        if (seconds <10)
        {
            seconds = '0'+seconds;
        }
        return `${hour}:${minutes}:${seconds}`;

    };

    const hourMinuteConstructor = function () {
        let date = new Date();
        let hour = date.getHours();
        let minutes = date.getMinutes();
        if(hour < 10)
        {
            hour = '0'+hour;
        }
        if (minutes <10)
        {
            minutes = '0'+minutes;
        }
        return `${hour}:${minutes}`;
    };

    $('#month-select').on('change',function () {
         let month = $('#month-select').val();
         let year = $('#year-select').val();
         sessionStorage.setItem('open',true);
         location = `http://localhost:8000/index/index?month=${month}&year=${year}`;
    });

    $('#year-select').on('change',function () {
        let month = $('#month-select').val();
        let year = $('#year-select').val();
        sessionStorage.setItem('open',true);
        location = `http://localhost:8000/index/index?month=${month}&year=${year}`;
    });

    let regex = new RegExp('[0-9][0-9]:[0-9][0-9]');
    $('.edit-start_time').on('keyup',function(){
        if (!$(this).val().match(regex) ||  $(this).val().length > 5) {
            $(this).css('background-color','red');
        }
        else{
            $(this).css('background-color','');
            let time = $(this).val();
            let id = $(this).attr('data-start-time-id');
            let user_id = $(this).attr('data-start-user-id');
            let date = $(this).attr('data-edit-start-date');
            let day = $(this).attr('data-day');
            $.ajax({
                url : `http://localhost:8000/edit/time/${id}`,
                method: 'POST',
                data: {'state' : 'start','time' : time,'id' : id, 'user_id' : user_id,'date' : date},
                success : function () {
                    $.ajax({
                        url:`http://localhost:8000/time/total/${user_id}/${date}`,
                        method: 'POST',
                        success: function (total_time) {
                            total = total_time.substr(1,5);
                            $(`.total-time-${user_id}-${day}:last`).text(`${total}`);
                        }
                    });
                }
            });
        }
    });

    $('.edit-stop_time').on('keyup',function(){
        if (!$(this).val().match(regex) ||  $(this).val().length > 5) {
            $(this).css('background-color','red');
        }
        else{
            $(this).css('background-color','');
            let time = $(this).val();
            let id = $(this).attr('data-stop-time-id');
            let user_id = $(this).attr('data-stop-user-id');
            let date = $(this).attr('data-edit-stop-date');
            let day = $(this).attr('data-day');
            $.ajax({
                url : `http://localhost:8000/edit/time/${id}`,
                method: 'POST',
                data: {'state' : 'stop','time' : time,'id' : id,'user_id' : user_id,'date' : date},
                success : function () {
                    $.ajax({
                        url:`http://localhost:8000/time/total/${user_id}/${date}`,
                        method: 'POST',
                        success: function (total_time) {
                            total = total_time.substr(1,5);
                            $(`.total-time-${user_id}-${day}:last`).text(`${total}`);
                        }
                    });
                }
            });
        }
    });

        $('.holiday-day').on('click',function () {
            if ($(this).attr('data-holiday') === 'true')
            {
                $.ajax({
                   url:'http://localhost:8000/holiday/destroy',
                   method: 'POST',
                   data : {'date' : $(this).attr('data-date')},
                    success : function () {
                        location = 'http://localhost:8000/holiday/index';
                    }
                });
            }else
                {
                    let date = $(this).attr('data-date');
                    let modal = $(``);
                    let year = $(this).attr('data-year');
                    let month = $(this).attr('data-month');
                    let day = $(this).attr('data-day');
                    $('#holiday-date-title').append(` ${date}`);
                    $('#date').attr('value',date);
                    $('#year').attr('value',year);
                    $('#month').attr('value',month);
                    $('#day').attr('value',day);
                    $('#add-holiday-modal').attr('hidden',false);
                    $('#open-modal').modal('show');
                }
        });

        $('#modal-close').on('click',function () {
            $('#open-modal').modal('hide');
        });
});