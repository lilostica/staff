-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: staff
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `start_time`
--

DROP TABLE IF EXISTS `start_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `start_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` varchar(45) NOT NULL,
  `start_date_time` varchar(45) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `start_time` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `start_time`
--

LOCK TABLES `start_time` WRITE;
/*!40000 ALTER TABLE `start_time` DISABLE KEYS */;
INSERT INTO `start_time` VALUES (1,'2020-12-01','2020-12-01 08:43:57',1,'08:43'),(2,'2020-12-01','2020-12-01 08:43:59',2,'08:43'),(3,'2020-12-01','2020-12-01 08:44:00',3,'08:44'),(4,'2020-12-01','2020-12-01 08:44:01',12,'08:44'),(5,'2020-12-01','2020-12-01 08:44:02',13,'08:44'),(6,'2020-12-01','2020-12-01 08:44:03',14,'08:44'),(7,'2020-12-01','2020-12-01 08:44:03',15,'08:44'),(8,'2020-12-01','2020-12-01 08:44:04',16,'08:44'),(9,'2020-12-01','2020-12-01 08:44:05',17,'08:44'),(10,'2020-12-01','2020-12-01 08:44:06',18,'08:44'),(11,'2020-12-01','2020-12-01 08:44:06',19,'08:44'),(12,'2020-12-02','2020-12-02 09:42:17',1,'09:42'),(13,'2020-12-02','2020-12-02 09:42:22',3,'09:42');
/*!40000 ALTER TABLE `start_time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stop_time`
--

DROP TABLE IF EXISTS `stop_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stop_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stop_date` varchar(48) NOT NULL,
  `stop_date_time` varchar(48) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `stop_time` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stop_time`
--

LOCK TABLES `stop_time` WRITE;
/*!40000 ALTER TABLE `stop_time` DISABLE KEYS */;
INSERT INTO `stop_time` VALUES (1,'2020-12-01','2020-12-01 08:45:08',19,'08:45'),(2,'2020-12-01','2020-12-01 08:45:08',18,'08:45'),(3,'2020-12-01','2020-12-01 08:45:09',17,'08:45'),(4,'2020-12-01','2020-12-01 08:45:10',16,'08:45'),(5,'2020-12-01','2020-12-01 08:45:10',15,'08:45'),(6,'2020-12-01','2020-12-01 08:45:11',14,'08:45'),(7,'2020-12-01','2020-12-01 08:45:12',13,'08:45'),(8,'2020-12-01','2020-12-01 08:45:12',12,'08:45'),(9,'2020-12-01','2020-12-01 08:45:13',3,'08:45'),(10,'2020-12-01','2020-12-01 08:45:14',2,'08:45'),(11,'2020-12-01','2020-12-01 08:45:15',1,'08:45'),(12,'2020-12-02','2020-12-02 09:42:18',1,'09:42'),(13,'2020-12-02','2020-12-02 09:42:23',3,'09:42');
/*!40000 ALTER TABLE `stop_time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `remember_token` varchar(128) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `login` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Ilyar Islamov','kenny01@inbox.ru','64814a3b7fd8444a56ad3641fd3451c6deaf0757',1,NULL,NULL,NULL,'Login1'),(2,'Vasya Petrov','test@test.ru','64814a3b7fd8444a56ad3641fd3451c6deaf0757',1,NULL,NULL,NULL,'Login2'),(3,'Jacob Leader','Test2@gmail.com','64814a3b7fd8444a56ad3641fd3451c6deaf0757',1,NULL,NULL,NULL,'Login13'),(12,'John Doe','email@email.ru','64814a3b7fd8444a56ad3641fd3451c6deaf0757',1,NULL,NULL,NULL,'Login14'),(13,'Jack Daniel','norma@mail.ru','141f87be1330a105a87923f4ee6383bd7de46541',1,NULL,NULL,NULL,'norma'),(14,'Barry Lie','new@mail.ru','141f87be1330a105a87923f4ee6383bd7de46541',1,NULL,NULL,NULL,'New LOgin'),(15,'Inpending Doom','hrytro@bail.cu','141f87be1330a105a87923f4ee6383bd7de46541',1,NULL,NULL,NULL,'qwerttyytytytyt'),(16,'Lu luis','hrytwro@bail.cu','64814a3b7fd8444a56ad3641fd3451c6deaf0757',1,'',NULL,NULL,'LLLooggin'),(17,'Ressti Res','newnew@mail.ru','64814a3b7fd8444a56ad3641fd3451c6deaf0757',1,NULL,NULL,NULL,'Kilooo'),(18,'Reborn Res','newnewqew@mail.ru','64814a3b7fd8444a56ad3641fd3451c6deaf0757',1,NULL,NULL,NULL,'qweqwewqeqw'),(19,'Inpending Doom','newnsssew@mail.ru','64814a3b7fd8444a56ad3641fd3451c6deaf0757',1,NULL,NULL,NULL,'qweqwewqeqw');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-08  9:44:19
