<?php if (($this->session->get('is_login'))) { ?>
    <div class="container">
        <div style="margin: 20px"  class="row row-cols-2">
            <div class="col">
        <select class="custom-select">
            <option selected>Open this select menu</option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>

        </select>
            </div>
            <div class="col">
                <select class="custom-select">
                    <option selected>Open this select menu</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>

                </select>
            </div>
        </div>
    </div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th id="test" scope="col">Hide/Show</th>
        <?php foreach ($users as $user) { ?>
            <th scope="row"><?= $user->name ?><?= $user->id ?></th>
        <?php } ?>
    </tr>
    </thead>
    <tbody id="test-hide">
    <?php foreach ($days as $day) { ?>
        <tr>
            <th scope="row"><?= $day->format('d D') ?></th>
            <?php foreach ($users as $user) { ?>
            <th scope="row">
                <p>User id: <?= $user->name ?> <?= $user->id ?></p><br>
                <input checked type="checkbox" aria-label="Checkbox for following text input"><br>
                <div class="row row-cols-2">
                    <div class="col">
                        Start
                <?php foreach ($user->start_times as $time) { ?> <?php if (($day->format('Y-m-d') === $time->start_date)) { ?> <?= $time->start_time ?> <?php } ?><?php } ?>
                    </div>
                    <div class="col">
                        Stop
                <?php foreach ($user->stop_times as $time) { ?> <?php if (($day->format('Y-m-d') === $time->stop_date)) { ?> <?= $time->stop_time ?>  <?php } ?><?php } ?>
                    </div>
                </div>
                <button id="start-time-<?= $user->id ?>-<?= $day->format('d') ?>"  data-day="<?= $day->format('Y-m-d') ?>" data-day-id="<?= $day->format('d') ?>" data-user-id="<?= $user->id ?>" type="button" class="btn btn-secondary btn-sm start-time">Start</button>
                <button  style="display: none"  id="stop-time-<?= $user->id ?>-<?= $day->format('d') ?>"  data-day="<?= $day->format('Y-m-d') ?>" data-day-id="<?= $day->format('d') ?>" data-user-id="<?= $user->id ?>" type="button" class="btn btn-secondary btn-sm stop-time">Stop</button>
            </th>
            <?php } ?>
        </tr>
    <?php } ?>
</table>
<?php } else { ?>
    <div class="container">
<p style="text-align: center; margin-top: 40px; font-size: 150px;"><?= $this->tag->linkTo(['session/login', 'class' => 'nav-link', 'LOGIN']) ?></p>
    </div>
<?php } ?>
