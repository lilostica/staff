
    <div class="container">
        <?php $isPasswordValidClass = ($form->messages('password') ? 'form-control is-invalid' : 'form-control'); ?>
        <?php $isConfigPasswordValidClass = ($form->messages('confirmPassword') ? 'form-control is-invalid' : 'form-control'); ?>

        <h1 class="mt-3">Change Password</h1>

        <?= $this->flash->output() ?>

        <form method="post">
            <div class="form-group row">
                <?= $form->label('password', ['class' => 'col-sm-2 col-form-label']) ?>
                <div class="col-sm-10">
                    <?= $form->render('password', ['class' => $isPasswordValidClass, 'placeholder' => 'Password']) ?>
                    <div class="invalid-feedback">
                        <?= $form->messages('password') ?>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <?= $form->label('confirmPassword', ['class' => 'col-sm-2 col-form-label']) ?>
                <div class="col-sm-10">
                    <?= $form->render('confirmPassword', ['class' => $isConfigPasswordValidClass, 'placeholder' => 'Confirm Password']) ?>
                    <div class="invalid-feedback">
                        <?= $form->messages('confirmPassword') ?>
                    </div>
                </div>
            </div>
                    <?= $form->render('Change') ?>
        </form>
    </div>