<table class="table table-bordered">
    <thead>
    <tr>
        <th id="test" scope="col">Hide/Show</th>
        <?php foreach ($users as $user) { ?>
            <th scope="row"><?= $user->name ?><?= $user->id ?></th>
        <?php } ?>
    </tr>
    </thead>
    <tbody id="test-hide">
    <?php foreach ($days as $day) { ?>
        <tr>
            <th scope="row"><?= $day->format('d D') ?></th>
            <?php foreach ($users as $user) { ?>
                <th  scope="row">
                    <input type="checkbox" aria-label="Checkbox for following text input"><br>
                    <?php if (($day->format('D') != 'Sat' && $day->format('D') != 'Sun')) { ?>
                        <p><?= $user->name ?></p><br>
                        <div id="time-block-<?= $user->id ?>-<?= $day->format('d') ?>">
                            <?php foreach ($user->times as $time) { ?>
                                <?php if (($time->date === $day->format('Y-m-d'))) { ?>
                                    <div class="row row-cols-2 stop-<?= $user->id ?>-<?= $day->format('d') ?>">
                                    <div class="col">
                                        <?php if (($this->session->get('is_login')['is_admin'])) { ?>
                                            <input  data-day="<?= $day->format('d') ?>" data-edit-start-date="<?= $time->date ?>" data-start-user-id="<?= $user->id ?>" data-start-time-id="<?= $time->id ?>" class="edit-start_time" type="text" style="width: 60px;" value="<?= $time->start_time ?>">
                                            <?php } else { ?>
                                                <?= $time->start_time ?>
                                        <?php } ?>
                                    </div>
                                    <?php if (($time->end_time)) { ?>
                                        <div class="col">
                                        <?php if (($this->session->get('is_login')['is_admin'])) { ?>
                                            <input data-day="<?= $day->format('d') ?>" data-edit-stop-date="<?= $time->date ?>" data-stop-user-id="<?= $user->id ?>" data-stop-time-id="<?= $time->id ?>" class="edit-stop_time" type="text" style="width: 60px;" value="<?= $time->end_time ?>">
                                        <?php } else { ?>
                                            <?= $time->end_time ?>
                                        <?php } ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="row row-cols-2 ">
                            <div class="col">
                                Total:
                            </div>
                            <?php if (($user->times->count() !== 0)) { ?>
                                <?php if (($time_nov->format('d D') >= $day->format('d D'))) { ?>
                                    <div class="col total-time-<?= $user->id ?>-<?= $day->format('d') ?>">
                                        <?php if (($time->totalDayTime($user->id, $day->format('Y-m-d')) !== '0:0')) { ?>
                                            <?= $time->totalDayTime($user->id, $day->format('Y-m-d')) ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            <?php } else { ?>
                                <div  class="col total-time-<?= $user->id ?>-<?= $day->format('d') ?>">
                                </div>
                            <?php } ?>
                        </div>
                        <?php if (($this->session->get('is_login')['is_admin'])) { ?>

                            <?php if (($user->buttonState($user->id))) { ?>
                                <button  id="start-time-<?= $user->id ?>-<?= $day->format('d') ?>"  data-day="<?= $day->format('Y-m-d') ?>" data-day-id="<?= $day->format('d') ?>" data-user-id="<?= $user->id ?>" type="button" class="btn btn-secondary btn-sm start-time">Start</button>
                                <button style="display: none;" id="stop-time-<?= $user->id ?>-<?= $day->format('d') ?>"  data-day="<?= $day->format('Y-m-d') ?>" data-day-id="<?= $day->format('d') ?>" data-user-id="<?= $user->id ?>" type="button" class="btn btn-secondary btn-sm stop-time">Stop</button>
                            <?php } else { ?>
                                <button  id="stop-time-<?= $user->id ?>-<?= $day->format('d') ?>"  data-day="<?= $day->format('Y-m-d') ?>" data-day-id="<?= $day->format('d') ?>" data-user-id="<?= $user->id ?>" type="button" class="btn btn-secondary btn-sm stop-time">Stop</button>
                                <button  style="display: none;" id="start-time-<?= $user->id ?>-<?= $day->format('d') ?>"  data-day="<?= $day->format('Y-m-d') ?>" data-day-id="<?= $day->format('d') ?>" data-user-id="<?= $user->id ?>" type="button" class="btn btn-secondary btn-sm start-time">Start</button>
                            <?php } ?>




                        <?php } else { ?>
                            <?php if (($time_nov->format('Y-m-d') == $day->format('Y-m-d') && $this->session->get('is_login')['id'] == $user->id)) { ?>
                                <?php if (($user->buttonState($user->id))) { ?>
                                    <button  id="start-time-<?= $user->id ?>-<?= $day->format('d') ?>"  data-day="<?= $day->format('Y-m-d') ?>" data-day-id="<?= $day->format('d') ?>" data-user-id="<?= $user->id ?>" type="button" class="btn btn-secondary btn-sm start-time">Start</button>
                                    <button style="display: none;" id="stop-time-<?= $user->id ?>-<?= $day->format('d') ?>"  data-day="<?= $day->format('Y-m-d') ?>" data-day-id="<?= $day->format('d') ?>" data-user-id="<?= $user->id ?>" type="button" class="btn btn-secondary btn-sm stop-time">Stop</button>
                                <?php } else { ?>
                                    <button  id="stop-time-<?= $user->id ?>-<?= $day->format('d') ?>"  data-day="<?= $day->format('Y-m-d') ?>" data-day-id="<?= $day->format('d') ?>" data-user-id="<?= $user->id ?>" type="button" class="btn btn-secondary btn-sm stop-time">Stop</button>
                                    <button  style="display: none;" id="start-time-<?= $user->id ?>-<?= $day->format('d') ?>"  data-day="<?= $day->format('Y-m-d') ?>" data-day-id="<?= $day->format('d') ?>" data-user-id="<?= $user->id ?>" type="button" class="btn btn-secondary btn-sm start-time">Start</button>
                                <?php } ?>
                                <?php } ?>
                        <?php } ?>
                        <br>
                    <?php } ?>
                </th>
            <?php } ?>
        </tr>
    <?php } ?>
</table>