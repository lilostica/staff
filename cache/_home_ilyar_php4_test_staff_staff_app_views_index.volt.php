<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Staff</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <style>

        </style>
    </head>
    <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <?= $this->tag->linkTo(['index', 'class' => 'navbar-brand', 'STAFF']) ?>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            </ul>

            <ul class="navbar-nav my-2 my-lg-0"><?php if ($this->session->get('is_login')) { ?><li style="margin-top: 8px; color: white;" class="nav-item">
                        <?= $this->session->get('is_login')['name'] ?>
                    </li>
                    <li style="margin-top: 8px; margin-left: 10px; color: grey; " class="nav-item">
                        <?= $this->tag->linkTo(['users/resetPassword', 'Change Password', 'style' => 'text-decoration: none; color: white;']) ?>
                    </li>
                    <li class="nav-item"><?= $this->tag->linkTo(['session/logout', 'class' => 'nav-link', 'Logout', 'style' => 'text-decoration: none; color: white;']) ?></li>
                <?php } else { ?>
                    <li class="nav-item"><?= $this->tag->linkTo(['session/login', 'class' => 'nav-link', 'Login']) ?></li>
                <?php } ?>
            </ul>
        </div>
    </nav>
    <?= $this->flash->output() ?>

            <?php echo $this->getContent(); ?>

        <!-- jQuery first, then Popper.js, and then Bootstrap's JavaScript -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script type="text/javascript" src="../js/table.js"></script>
    </body>
</html>
