<div  style="text-align: center" class="jumbotron">
    <h1>Unauthorized</h1>
    <p>You don't have access to this option. Contact an administrator</p>
    <p><?= $this->tag->linkTo(['login', 'Login', 'class' => 'btn btn-primary']) ?></p>
</div>
