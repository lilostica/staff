<div class="page-header">
    <h1 style="text-align: center">Articles</h1>
</div>
<div class="container">
    <div  class="row row-cols-4">
        <?php foreach ($articles as $article) { ?>
            <div style="margin: 10px;" class="col">
                <div class="card" style="width: 20rem;">
                    <div class="card-body">
                        <h5 class="card-title"><?= $article->title ?></h5>
                        <h6 class="card-subtitle mb-2 text-muted">Author: <?= $article->user->name ?></h6>
                        <p class="card-text"><?= $article->content ?></p>
<!--                        <a href="--><?//= $this->url->get('/article/'->article->id) ?><!--" class="card-link">Edit</a>-->
                        <a href="#" class="card-link">Another link</a>
                    </div>
                </div>
                
                
            </div>
        <?php } ?>

    </div>

