
<div class="container">
    <div hidden id="add-holiday-modal">
        <div id="open-modal" class="modal" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 id="holiday-date-title" class="modal-title">Add Holiday</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="<?= $this->url->get('holiday/store') ?>">
                            <div class="mb-3">
                                <label for="name" class="form-label">Holiday Name</label>
                                <input name="name" type="text" class="form-control" id="name" aria-describedby="name">
                                <input hidden name="year" type="text" class="form-control" id="year" aria-describedby="year">
                                <input hidden name="month" type="text" class="form-control" id="month" aria-describedby="month">
                                <input hidden name="day" type="text" class="form-control" id="day" aria-describedby="day">
                                <input hidden name="date" type="text" class="form-control" id="date" aria-describedby="date">
                            </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="modal-close" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h1 style="text-align: center; margin: 25px;">Holiday Calendar <?= $current_date->format('Y') ?></h1>
    <div class="row">
    <?php foreach ($months as $month) { ?>
        <div   class="col-3">
            <h3 style="text-align: center"><?= $month->format('M') ?></h3>
            <div class="card">
                <div class="card-body" style="width: 300px;">
                    <table>
                        <thead>
                        <tr>
                            <?php foreach ($weeks_day as $week) { ?>

                            <th <?php if (($week === 'Sat' || $week == 'Sun')) { ?> style="background-color: lightblue;"<?php } ?> style="width: 30px;"> <?= $week ?></th>
                            <?php } ?>
                        </tr>
                        </thead>
                            <tbody>
                            <tr>
                                <?php foreach ($weeks_day as $week) { ?>
                                    <td <?php if (($week === 'Sat' || $week == 'Sun')) { ?> style="background-color: lightblue;"<?php } ?>">
                                    <?php foreach ($days as $day) { ?>
                                        <?php if (($day->format('D') === $week && $day->format('m') == $month->format('m'))) { ?>
                                         <div <?php if (($holiday_validate !== null)) { ?>
                                                <?php if (($holiday_validate->findHoliday($day->format('Y-m-d')))) { ?>
                                                 style="cursor: pointer; background-color: red" data-holiday="true"
                                                 <?php } else { ?>
                                                 style="cursor: pointer;"
                                                 <?php } ?>
                                              <?php } ?> data-year="<?= $day->format('Y') ?>" data-month="<?= $day->format('m') ?>"  data-day="<?= $day->format('d') ?>"  data-date="<?= $day->format('Y-m-d') ?>" class="holiday-day">
                                                   <?= $day->format('d') ?>
                                               </div>
                                        <?php } ?>
                                    <?php } ?>
                                    </td>
                                <?php } ?>
                            </tr>
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php } ?>
    </div>
    <div style="text-align: center; margin: 50px;" id="holidays-block">
        <div class="row">
            <?php foreach ($all_holidays as $holiday) { ?>
                <div class="col-3">
                    <?= $holiday->date ?> - <?= $holiday->name ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
