<?php if (($this->session->get('is_login')['is_admin'])) { ?>
    <div style="text-align: center" class="container">
        <div class="row">
            <div class="col">
                <p><?= $this->tag->linkTo(['user/add', 'Add user', 'style' => ' color: black; font-size: 20px;', 'class' => 'btn btn-link']) ?></p>
            </div>
            <div class="col-6">
                <p><?= $this->tag->linkTo(['holiday/index', 'Add holiday', 'style' => ' color: black; font-size: 20px;', 'class' => 'btn btn-link']) ?></p>
            </div>
        </div>
    </div>
<?php } ?>
    <?php $i = 0; ?>
    <div  style="width: 850px; margin: 20px;">
        <p>You have: <?= $all_time ?> </p>
        <p>You have/Assigned: <?= $assigned_proc ?>% </p>
        <p>Assigned: <?= $work_days ?></p>
        <p>Ты опоздал: <?= $late ?> </p>
        <p>На работе необходимо быть до 9:00. Если опоздали больше 3х раз в месяц, то дисциплина будет считаться не удовлетворительной и негативно скажется на запрос по повышению оклада.</p>
    </div>
    <div class="container">
        <div style="margin: 20px"  class="row row-cols-2">
            <div class="col">
        <select id="month-select" class="custom-select">
            <?php foreach ($month_select as $month) { ?>
                <?php $i += 1; ?>
                    <option <?php if (($current_month->format('M') == $login_user->sliceMonthName($month))) { ?> selected <?php } ?> value="<?= $i ?>"><?= $month ?></option>
            <?php } ?>
        </select>
            </div>
            <div class="col">
                <select id="year-select" class="custom-select">
                  <?php foreach ($current_year as $year) { ?>
                      <option <?php if (($current_month->format('Y') == $year)) { ?> selected <?php } ?> value="<?= $year ?>"><?= $year ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
<?= $this->partial('index/partials/table') ?>
