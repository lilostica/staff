<div class="page-header">
    <h1 style="text-align: center"><?= $article->title ?></h1>
</div>
<div  class="container">
    <h5 style="text-align: center" class="card-title">Author: <?= $article->user->name ?></h5>
    <div style="text-align: center" class="">
            <div style="margin: 10px;" class="col">
                <div class="card" style="width: 1000px;">
                    <div class="card-body">
                        <p class="card-text"><?= $article->content ?></p>
                        <a href="#" class="card-link"><?= $this->tag->linkTo(['articles/index/', '<i class="icon-pencil"></i> Back', 'class' => 'btn btn-sm btn-outline-warning']) ?></a>
                    </div>
                </div>
            </div>
    </div>
</div>