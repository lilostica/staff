<table class="table table-bordered">
    <thead>
    <tr>
        <th id="test" scope="col">Hide/Show</th>
        <?php foreach ($users as $user) { ?>
            <th scope="row"><?= $user->name ?><?= $user->id ?></th>
        <?php } ?>
    </tr>
    </thead>
    <tbody id="test-hide">
    <?php foreach ($days as $day) { ?>
        <tr>
            <th scope="row"><?= $day->format('d D') ?></th>
            <?php foreach ($users as $user) { ?>
                <th  scope="row">
                    <input type="checkbox" aria-label="Checkbox for following text input"><br>
                    <?php if (($day->format('D') != 'Sat' && $day->format('D') != 'Sun')) { ?>
                        <p><?= $user->name ?></p><br>
                        <div id="time-block-<?= $user->id ?>-<?= $day->format('d') ?>">
                            <?php foreach ($user->times as $time) { ?>
                                <?php if (($time->date === $day->format('Y-m-d'))) { ?>
                                    <div class="row row-cols-2 stop-<?= $user->id ?>-<?= $day->format('d') ?>">
                                    <div class="col">
                                        <?= $time->start_time ?>
                                    </div>
                                    <?php if (($time->end_time)) { ?>
                                        <div class="col">
                                            <?= $time->end_time ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="row row-cols-2 ">
                            <div class="col">
                                Total:
                            </div>
                            <?php if (($user->times->count() !== 0)) { ?>
                                <?php if (($time_nov->format('d D') >= $day->format('d D'))) { ?>
                                    <div class="col total-time-<?= $user->id ?>-<?= $day->format('d') ?>">
                                        <?php if (($time->totalDayTime($user->id, $day->format('Y-m-d')) !== '0:0')) { ?>
                                            <?= $time->totalDayTime($user->id, $day->format('Y-m-d')) ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            <?php } else { ?>
                                <div class="col total-time-<?= $user->id ?>-<?= $day->format('d') ?>">
                                </div>
                            <?php } ?>
                        </div>
                        <?php if (($time_nov->format('d D') == $day->format('d D'))) { ?>
                            <?php if (($user->buttonState($user->id))) { ?>
                                <button  id="start-time-<?= $user->id ?>-<?= $day->format('d') ?>"  data-day="<?= $day->format('Y-m-d') ?>" data-day-id="<?= $day->format('d') ?>" data-user-id="<?= $user->id ?>" type="button" class="btn btn-secondary btn-sm start-time">Start</button>
                                <button style="display: none;" id="stop-time-<?= $user->id ?>-<?= $day->format('d') ?>"  data-day="<?= $day->format('Y-m-d') ?>" data-day-id="<?= $day->format('d') ?>" data-user-id="<?= $user->id ?>" type="button" class="btn btn-secondary btn-sm stop-time">Stop</button>
                            <?php } else { ?>
                                <button  id="stop-time-<?= $user->id ?>-<?= $day->format('d') ?>"  data-day="<?= $day->format('Y-m-d') ?>" data-day-id="<?= $day->format('d') ?>" data-user-id="<?= $user->id ?>" type="button" class="btn btn-secondary btn-sm stop-time">Stop</button>
                                <button  style="display: none;" id="start-time-<?= $user->id ?>-<?= $day->format('d') ?>"  data-day="<?= $day->format('Y-m-d') ?>" data-day-id="<?= $day->format('d') ?>" data-user-id="<?= $user->id ?>" type="button" class="btn btn-secondary btn-sm start-time">Start</button>
                            <?php } ?>
                        <?php } else { ?>
                            <?php if (($time_nov->format('d D') > $day->format('d D'))) { ?>
                                <button class="btn btn-link btn-sm">Edit</button>
                            <?php } ?>
                        <?php } ?>
                        <br>
                    <?php } ?>
                </th>
            <?php } ?>
        </tr>
    <?php } ?>
</table>