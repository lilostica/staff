<?php

use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Select;
use \Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class UpdateUserForm extends Form
{
    public function initialize($entity = null, array $options = [])
    {

        $name = new Text('name');
        $name->setLabel('Name');
        $name->addValidators([
            new PresenceOf([
                'message' => 'The name is required',
            ]),
        ]);

        $this->add($name);

        // Email
        $email = new Text('email');
        $email->setLabel('E-Mail');
        $email->addValidators([
            new PresenceOf([
                'message' => 'The e-mail is required',
            ]),
        ]);
        $this->add($email);


        $login = new Text('login');
        $login->setLabel('Login');
        $login->addValidators([
            new PresenceOf([
                'message' => 'The login is required',
            ]),
        ]);

        $this->add($login);

        $is_active = new Select('is_active', [
             true => 'Active',
             false =>'Deactivate',
        ]);
        $this->add($is_active);

        $is_admin = new Select('is_admin', [
            true => 'Admin',
            false =>'User',
        ]);
        $this->add($is_admin);
    }

    /**
     * Prints messages for a specific element
     *
     * @param string $name
     *
     * @return string
     */
    public function messages(string $name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                return $message;
            }
        }

        return '';

    }
}