<?php
declare(strict_types=1);

use \Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator;


class Users extends Model
{
    public $id;

    public $name;

    public $email;

    public $login;

    public $is_active;

    public  $is_admin;

    protected function validation()
    {
        $validation = new Validation();

        $validation->add(
            'email',
            new Validator\Uniqueness(['message' => 'This email already exists'])
        );

        $validation->add(
            'login',
            new Validator\Uniqueness(['message' => 'This login already exists'])
        );

        return $this->validate($validation);
    }

    public function initialize()
    {
        $this->hasMany('id', Times::class, 'user_id', [
            'alias'    => 'times',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getConnectionService()
    {
        // TODO: Implement getConnectionService() method.
    }

    /**
     * @inheritDoc
     */
    public function getConnection()
    {
        // TODO: Implement getConnection() method.
    }

    /**
     * @inheritDoc
     */
    public function dumpResult($base, $result)
    {
        // TODO: Implement dumpResult() method.
    }

    /**
     * @inheritDoc
     */
    public function setForceExists($forceExists)
    {
        // TODO: Implement setForceExists() method.
    }

    public function buttonState(int $id)
    {
        $user = Users::findFirst(['conditions' => 'id = :id:',
            'bind' => ['id' => $id
            ]]);
        if ($user->times->count() === 0)
        {
            $count_state = 'not_records';
        }
        if (strlen((string) $user->times->getLast()->end_time) === 0 && $count_state !== 'not_records')
        {
            return false;
        }
        else {

            return true;
        }
    }
    public function sliceMonthName($month)
    {
        return "{$month[0]}{$month[1]}{$month[2]}";
    }

    public function getUpdateUrl($id)
    {
        return "user/update/{$id}";
    }

    public static function getAllUsersFirstAuthUser($login_user,$all_users)
    {
        $all_users_first_login_user = [];
        $all_users_first_login_user [] = $login_user;
        foreach ($all_users as $user)
        {
            if ($user->id !== $login_user->id)
            {
                $all_users_first_login_user[] = $user;
            }
        }
        return $all_users_first_login_user;
    }
}