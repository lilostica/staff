<?php
use  Phalcon\Mvc\Model;

class Times extends Model
{

    public $id;

    public $user_id;

    public $start_time;

    public $end_time;

    public $date;

    public function initialize()
    {
        $this->belongsTo('user_id',Users::class,'id',[
            'alias' => 'user'
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getConnectionService()
    {
        // TODO: Implement getConnectionService() method.
    }

    /**
     * @inheritDoc
     */
    public function getConnection()
    {
        // TODO: Implement getConnection() method.
    }

    /**
     * @inheritDoc
     */
    public function dumpResult($base, $result)
    {
        // TODO: Implement dumpResult() method.
    }

    /**
     * @inheritDoc
     */
    public function setForceExists($forceExists)
    {
        // TODO: Implement setForceExists() method.
    }

    public static function Calendar($month = null,$year = null)
    {
        if ($month == null)
        {
            $current_month = (int) date('m');
        }else
            {
                $current_month = $month;
            }
        if ($year == null)
        {
            $current_year = (int) date('Y');
        }else
        {
            $current_year = $year;
        }
        $array_days = [];
        $count_days_of_month = date('t',mktime(0, 0, 0, $current_month,1,$current_year));
        for ($i = 1; $i<=$count_days_of_month;$i++ )
        {
            $date = new DateTime();
            $date->setDate($current_year,$current_month,$i);
            $array_days[] = $date;
        }
        return $array_days;
    }

    public static function addStartOrStopTime(string $date,int $user_id, string  $state)
    {
        $date_latest = new DateTime();
        $date_latest->setTimezone(new DateTimeZone('Asia/Bishkek'));

        $new_date = new DateTime($date,new DateTimeZone('Asia/Bishkek'));
        $full_date = $new_date->format('Y-m-d');
        $time = $new_date->format('H:i');
        if ($state == 'start')
        {
            if (strtotime($time) > strtotime('09:00'))
            {
                $validate_late = Times::findFirst([
                    'conditions' => 'date = :date: And user_id = :user_id:',
                        'bind' =>[
                            'date' => $full_date,
                            'user_id' => $user_id
                        ]
                    ]
                );

                if ($validate_late !== null)
                {
                    $startTime = new Times([
                        'user_id' => $user_id,
                        'start_time' => $time,
                        'date' => $full_date,
                    ]);
                    $startTime->save();
                }else{
                    $startTime = new Times([
                        'user_id' => $user_id,
                        'start_time' => $time,
                        'date' => $full_date,
                        'late' => true
                    ]);
                    $startTime->save();
                }
            }else
                {
                    $startTime = new Times([
                        'user_id' => $user_id,
                        'start_time' => $time,
                        'date' => $full_date
                    ]);
                    $startTime->save();
                }
        }else
            {
                $user = Users::findFirst(['conditions' => 'id = :id:',
                    'bind' => ['id' => $user_id
                    ]]);
                $last_time = $user->times->getLast();
                if ($last_time->end_time == null)
                {
                    $last_time->end_time = $time;
                    $last_time->save();
                }
            }

    }

    public static function totalDayTime( $user_id,  $date)
    {
        $times = Times::find(
            ['conditions' => ['id = :user_id:','date' => ':date:'],
                'bind' => [
                    'id' => $user_id,
                    'date' => $date
                ]
            ]);
        $hours = null;
        $minutes = null;
        foreach ($times as $time)
        {
            if ($time->date == $date && $time->user_id == $user_id)
            {
                $start_date = new DateTime("{$time->date} {$time->start_time}",new DateTimeZone('Asia/Bishkek'));
                $end_date = new DateTime("{$time->date} {$time->end_time}",new DateTimeZone('Asia/Bishkek'));
                $dif_hours = $start_date->diff($end_date)->h;
                $dif_minutes = $start_date->diff($end_date)->i;
                $hours += $dif_hours;
                $minutes += $dif_minutes;
            }
        }
        if ($hours < 10)
        {
            $hours = "0{$hours}";
        }
        if ($minutes < 10)
        {
            $minutes = "0{$minutes}";
        }
        return  "{$hours}:{$minutes}";
    }

    public static function totalTime($user_id,$month = null,$year = null)
    {
        if ($month and $year !== null)
        {
            $date = new DateTime();
            $date->setDate($year,$month,1);
        }else
            {
                $date = new DateTime();
                $date->setTimezone(new DateTimeZone('Asia/Bishkek'));
            }

        $times = Times::find(
            ['conditions' => 'user_id = :user_id:',
                'bind' => [
                    'user_id' => $user_id
                ]
            ]);
        $hours = null;
        $minutes = null;
        foreach ($times as $time)
        {
            $time_date = new DateTime("{$time->date} {$time->start_time}",new DateTimeZone('Asia/Bishkek'));
            if ($date->format('Y-m') == $time_date->format('Y-m'))
            {
                $start_date = new DateTime("{$time->date} {$time->start_time}",new DateTimeZone('Asia/Bishkek'));
                $end_date = new DateTime("{$time->date} {$time->end_time}",new DateTimeZone('Asia/Bishkek'));
                $dif_hours = $start_date->diff($end_date)->h;
                $dif_minutes = $start_date->diff($end_date)->i;
                $hours += $dif_hours;
                $minutes += $dif_minutes;
            }
        }
        $proc_of_hour = round(($minutes *100) / 60);
        if ($hours < 10)
        {
            $hours = "0{$hours}";
        }
        if ($minutes < 10)
        {
            $minutes = "0{$minutes}";
        }
        return  "{$hours}.{$proc_of_hour}";
    }


    public static function latest($user_id,$month = null,$year = null)
    {
        $latest = Times::find(
            ['conditions' => 'user_id = :user_id: AND late = :late:',
                'bind' => [
                    'user_id' => $user_id,
                    'late' => true
                ]
            ]);
        $latest_count = null;

        foreach ($latest as $late )
        {
            $date = new DateTime("{$late->date} {$late->time}",new DateTimeZone('Asia/Bishkek'));
            $current_date = new DateTime();
            $current_date->setTimezone(new DateTimeZone('Asia/Bishkek'));
            if ($month and $year !== null)
            {
                $current_date = new DateTime();
                $current_date->setDate($year,$month,1);
            }
            if ($current_date->format('Y-m') == $date->format('Y-m'))
            {
                $latest_count +=1;
            }
        }

        return $latest_count;
    }

    public static function workDays($month = null,$year = null)
    {
        if ($month == null)
        {
            $current_month = (int) date('m');
        }else
        {
            $current_month = $month;
        }
        if ($year == null)
        {
            $current_year = (int) date('Y');
        }else
        {
            $current_year = $year;
        }
        if ($month == null)
        {
            $holidays = Holiday::find(['conditions' => 'month = :month: AND year = :year:','bind' => ['month' => date('m') , 'year' =>  date('Y')]]);
        }else
            {
                if ($month < 10)
                {
                    $month = "0".$month;
                }
                if ($year < 10)
                {
                    $year = "0".$month;
                }
                $holidays = Holiday::find(['conditions' => 'month = :month: AND year = :year:','bind' => ['month' =>  $month, 'year' =>     $year]]);
            }
        $holidays_count = $holidays->count();
        $holidays_hour = $holidays_count * 8;
        $array_days = [];
        $count_days_of_month = date('t',mktime(0, 0, 0, $current_month,1,$current_year));
        for ($i = 1; $i<=$count_days_of_month;$i++ )
        {
            $date = new DateTime();
            $date->setTimezone(new DateTimeZone('Asia/Bishkek'));
            $date->setDate($current_year,$current_month,$i);
            if ($date->format('D') !== 'Sat' and $date->format('D') !== 'Sun')
            {
                $array_days[] = $date;
            }
        }
        if ($holidays_count = 0){
            return  $count_hour = count($array_days) * 8;
        }
        $count_hour = count($array_days) * 8 - $holidays_hour;
        return $count_hour;
    }

    public static function getMonthArray()
    {
        return [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];
    }

    public static function getYearsArray()
    {
        $current_year = new DateTime();
        new DateTime();
        $c_year = (int) $current_year->format('Y');
        $y_arr = [];
        for ($i = 20; $i >= 0; $i --)
        {
            $y_arr[] = $c_year--;
        }
        return $y_arr;
    }
}