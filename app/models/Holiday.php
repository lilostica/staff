<?php
use \Phalcon\Mvc\Model;

class Holiday extends Model
{
    public $id;

    public $date;

    public $year;

    public $month;

    public $day;

    public $name;
    /**
     * @inheritDoc
     */
    public function getConnectionService()
    {
        // TODO: Implement getConnectionService() method.
    }

    /**
     * @inheritDoc
     */
    public function getConnection()
    {
        // TODO: Implement getConnection() method.
    }

    /**
     * @inheritDoc
     */
    public function dumpResult($base, $result)
    {
        // TODO: Implement dumpResult() method.
    }

    /**
     * @inheritDoc
     */
    public function setForceExists($forceExists)
    {
        // TODO: Implement setForceExists() method.
    }

    public static function HolidayCalendar()
    {
        $array_days = [];
        $array_moth = [];
        for($i = 1; $i<=12;$i ++)
        {
            $current_month = $i;
            $current_year = (int) date('Y');
            $count_days_of_month = date('t',mktime(0, 0, 0, $current_month,1,$current_year));
            $months = new DateTime();
            $array_moth[] = $months->setDate($current_year,$i,1);
            for ($j = 1; $j<= $count_days_of_month;$j++ )
            {
                $date = new DateTime();
                $date->setDate($current_year,$current_month,$j);
                $array_days[] = $date;
            }
        }
        return ['days' => $array_days, 'months' => $array_moth];
    }

    public static function weekDays()
    {
        return ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
    }

    public function findHoliday($date = null)
    {
        $holidays = Holiday::find();
        $holidays_array = [];
        foreach ($holidays as $holiday)
        {
            $holidays_array[]= $holiday->date;
        }
        $flip = array_flip($holidays_array);
        if(array_key_exists($date,$flip))
        {
            return true;
        }else{
            return false;
        }
    }
}