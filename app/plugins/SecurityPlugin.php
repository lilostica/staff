<?php


use \Phalcon\DI\Injectable;
use Phalcon\Acl\Component;
use Phalcon\Acl\Role;
use Phalcon\Acl\Enum;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl\Adapter\Memory as AclList;

class SecurityPlugin extends Injectable
{
    public function beforeExecuteRoute(Event $event,Dispatcher $dispatcher)
    {

        $auth = $this->session->get('is_login');

        if (!$auth || $auth['is_active'] == 0) {
            $role = 'Guests';
        } else {
            if ($auth['is_admin'])
            {
                $role = 'Admins';
            }else {
                $role = 'Users';
            }
        }

        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();

        $acl = $this->getAcl();

        if (!$acl->isComponent($controller)) {
            $dispatcher->forward([
                'controller' => 'errors',
                'action'     => 'show404',
            ]);

            return false;
        }

        $allowed = $acl->isAllowed($role, $controller, $action);

        if (!$allowed) {
            $dispatcher->forward([
                'controller' => 'errors',
                'action'     => 'show401',
            ]);
            $this->session->destroy();

            return false;
        }

        return true;
    }

    protected function getAcl(): AclList
    {
        if (isset($this->persistent->acl)) {
            return $this->persistent->acl;
        }

        $acl = new AclList();
        $acl->setDefaultAction(Enum::DENY);


        $roles = [
            'users'  => new Role(
                'Users',
                'Member privileges, after sign in.'
            ),
            'guests' => new Role(
                'Guests',
                'Anyone browsing the site who is not signed in is considered to be a "Guest".'
            ),
            'admins' => new Role(
                'Admins',
                'Member privileges, granted after sign in.'
            ),
        ];

        foreach ($roles as $role) {
            $acl->addRole($role);
        }

        $privateAdminResources = [
            'index'    => ['index'],
            'time'     => ['store', 'getTotalDayTime', 'edit'],
            'session'  => ['logout'],
            'users' =>['store','update','resetPassword'],
            'holiday' => ['index','store','destroy']
        ];
        foreach ($privateAdminResources as $resource => $actions) {
            $acl->addComponent(new Component($resource), $actions);
        }

        $privateUserResources = [
            'index'    => ['index'],
            'time'     => ['store', 'getTotalDayTime',],
            'session'  => ['logout'],
            'users' => ['resetPassword']
        ];

        foreach ($privateUserResources as $resource => $actions) {
            $acl->addComponent(new Component($resource), $actions);
        }

        $publicResources = [
            'session'    => ['login'],
            'errors' => ['show401']
        ];
        foreach ($publicResources as $resource => $actions) {
            $acl->addComponent(new Component($resource), $actions);
        }

        foreach ($roles as $role) {
            foreach ($publicResources as $resource => $actions) {
                foreach ($actions as $action) {
                    $acl->allow($role->getName(), $resource, $action);
                }
            }
        }

        foreach ($privateUserResources as $resource => $actions) {
            foreach ($actions as $action) {
                $acl->allow('Users', $resource, $action);
            }
        }

        foreach ($privateAdminResources as $resource => $actions) {
            foreach ($actions as $action) {
                $acl->allow('Admins', $resource, $action);
            }
        }

        $this->persistent->acl = $acl;
        return $acl;
    }
}