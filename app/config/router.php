<?php

$router = $di->getRouter();

$router->add('/', [
    'controller' => 'session',
    'action'     => 'login',
]);

$router->add('/index', [
    'controller' => 'index',
    'action'     => 'index',
]);

$router->add('/index/{month}/{year}', [
    'controller' => 'index',
    'action'     => 'index',
]);


$router->add('/user/add', [
    'controller' => 'users',
    'action'     => 'store',
]);

$router->add('/user/update/{id}', [
    'controller' => 'users',
    'action'     => 'update',
]);

$router->add('/user/reset/password/{id}', [
    'controller' => 'users',
    'action'     => 'resetPassword',
]);

$router->add('/time/start/or/stop', [
    'controller' => 'time',
    'action'     => 'store',
]);

$router->add('/time/total/{id}/{date}', [
    'controller' => 'time',
    'action'     => 'getTotalDayTime',
]);

$router->add('/edit/time/{id}', [
    'controller' => 'time',
    'action'     => 'edit',
]);

$router->add('/login', [
    'controller' => 'session',
    'action'     => 'login',
]);

$router->handle($_SERVER['REQUEST_URI']);
