<?php
declare(strict_types=1);

use Phalcon\Mvc\Controller;
use Vokuro\Plugins\Auth\Auth;
use Vokuro\Plugins\Auth\Exception as AuthException;

class SessionController extends ControllerBase
{
    public function indexAction(): void
    {
    }
    public function loginAction()
    {
        $form = new LoginForm();
        if ($this->session->get('is_login'))
        {
            $this->flash->error('Login already!');
            return $this->dispatcher->forward(array(
                'controller' => 'index',
                'action' => 'index'
            ));
        }else{
        if ($this->request->isPost()) {
            $email = $this->request->getPost('email', 'email');
            $password = $this->request->getPost('password');
            $password = sha1($password);
            $user = Users::findFirst([
                'conditions' => "email=:login: AND password=:pass:",
                'bind' => ['login' => $email, 'pass' => $password]
            ]);
            if ($user) {
                $this->flash->success('Welcome '.$user->name);
                $this->session->set('is_login',
                    [
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                        'is_admin' => $user->is_admin,
                        'is_active' => $user->is_active,
                    ]);
                return $this->response->redirect('index');
            }
            $this->flash->error('Wrong email/password');
        }
        $this->view->setVar('form', $form);
        }
    }

    public function logoutAction()
    {
        $this->session->remove('is_login');

        return $this->response->redirect('login');
    }
}