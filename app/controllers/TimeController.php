<?php

use\Phalcon\Mvc\Controller;

class TimeController extends Controller
{

    public function storeAction()
    {
        if ($this->session->get('is_login'))
        {
            if ($this->request->isPost())
            {
                $user_id = $this->request->get('user_id');
                $date = $this->request->get('date');
                $time =  $this->request->get('time');
                $time_state = $this->request->get('time_state');
                Times::addStartOrStopTime($date,$user_id,$time_state);

            }
        }else
            {
                return $this->response->redirect('/login');
            }


    }

    public function getTotalDayTimeAction($user_id,$date)
    {
        $total = Times::totalDayTime($user_id,$date);
        return json_encode($total);
    }

    public function editAction($id)
    {
        if ($this->session->get('is_login'))
        {
            if ($this->request->isPost())
            {
                $id = $this->request->get('id');
                $state = $this->request->get('state');
                $time = $this->request->get('time');
                $user_id = $this->request->get('user_id');
                $date = $this->request->get('date');
                $time_model = Times::findFirst(['conditions' => 'id = :id:','bind' => ['id' => $id]]);
                if ($state == 'start')
                {
                    $time_model->start_time = $time;
                    $time_model->save();
                    if (strtotime($time) > strtotime('09:00'))
                    {
                        $validate_late = Times::findFirst([
                                'conditions' => 'date = :date: And user_id = :user_id:',
                                'bind' =>[
                                    'date' => $date,
                                    'user_id' => $user_id
                                ]
                            ]
                        );
                        if (!$validate_late->late and strtotime($validate_late->start_time) > strtotime('09:00'))
                        {
                            $validate_late->late = 1;
                            $validate_late->save();
                        }
                    }else{
                        $time_model->late = 0;
                        $time_model->save();
                    }
                }else{
                    $time_model->end_time = $time;
                    $time_model->save();
                }
            }
        }else{
            return $this->response->redirect('/login');
        }
    }

}