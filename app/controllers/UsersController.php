<?php
use\Phalcon\Mvc\Controller;

class UsersController extends Controller
{

    public function index()
    {

    }

    public function storeAction()
    {
        $form = new CreateUserForm();
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {
                $user = new Users([
                    'name'       => $this->request->getPost('name', 'striptags'),
                    'login'       => $this->request->getPost('login', 'striptags'),
                    'email'      => $this->request->getPost('email'),
                    'password'   => sha1($this->request->getPost('password')),
                ]);

                if ($user->save()) {
                    $this->flash->success($user->name ." Has added!");
                }
                foreach ($user->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
            }
        }
        $this->view->setVar('form', $form);
    }

    public function updateAction($id)
    {
        $user = Users::findFirst(['conditions' => 'id = :id:','bind' => ['id' => $id]]);
        if (!$user) {
            $this->flash->error('User was not found.');

            return $this->dispatcher->forward([
                'controller' => 'users',
                'action' => 'store'
            ]);
        }
        $form = new UpdateUserForm($user, [
            'edit' => true,
        ]);

        if ($this->request->isPost()) {
            $user->assign([
                'name'       => $this->request->getPost('name', 'striptags'),
                'profilesId' => $this->request->getPost('login', 'striptags'),
                'email'      => $this->request->getPost('email', 'email'),
                'is_active'      => $this->request->getPost('is_active'),
                'is_admin'      => $this->request->getPost('is_admin'),
            ]);

            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
            } else {
                if (!$user->save()) {
                    foreach ($user->getMessages() as $message) {
                        $this->flash->error((string) $message);
                    }
                } else {
                    $this->flash->success('User was updated successfully.');
                }
            }
        }
        $this->view->setVar('user' ,$user);
        $this->view->setVar('form',$form);
    }

    public function resetPasswordAction()
    {
        $user = Users::findFirst(['conditions' => 'id = :id:', 'bind' => ['id' => $this->session->get('is_login')['id']]]);
        if (!$user) {
            $this->flash->error('User was not found.');

            return $this->dispatcher->forward([
                'controller' => 'users',
                'action' => 'resetPassword'
            ]);
        }

        $form = new ChangePasswordForm();

        if ($this->request->isPost()) {
            $user->assign([
                'password' => sha1($this->request->getPost('password', 'striptags')),
            ]);
        }

        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error((string)$message);
                }
            } else {
                if (!$user->save()) {
                    foreach ($user->getMessages() as $message) {
                        $this->flash->error((string)$message);
                    }
                } else {
                    $this->flash->success('Password was updated successfully.');
                }
            }
        }
        $this->view->setVar('form',$form);
    }
}