<?php
declare(strict_types=1);

/**
 * This file is part of the Invo.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Phalcon\Mvc\Controller;

/**
 * ErrorsController
 *
 * Manage errors
 */
class ErrorsController extends Controller
{
    public function show401Action(): void
    {
        return;
    }

    public function show404Action(): void
    {
        return;
    }
}
