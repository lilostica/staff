<?php

use \Phalcon\Mvc\Controller;

class HolidayController extends Controller
{

    public function indexAction()
    {
        $week_days = Holiday::weekDays();
        $calendar = Holiday::HolidayCalendar();
        if (Holiday::findFirst())
        {
            $holiday = Holiday::findFirst();
        }else{
            $holiday = null;
        }
        $all_holidays = Holiday::find();
        $current_date = new DateTime();
        $current_date->setTimezone(new DateTimeZone('Asia/Bishkek'));

        $this->view->setVar('days',$calendar['days']);
        $this->view->setVar('months',$calendar['months']);
        $this->view->setVar('weeks_day',$week_days);
        $this->view->setVar('weeks_day',$week_days);
        $this->view->setVar('holiday_validate',$holiday);
        $this->view->setVar('current_date',$current_date);
        $this->view->setVar('all_holidays',$all_holidays);
        return;
    }

    public function storeAction()
    {
        if ($this->request->isPost())
        {
            $holiday = new Holiday(
                [
                    'date' => $this->request->get('date'),
                    'year' => $this->request->get('year'),
                    'month' => $this->request->get('month'),
                    'day' => $this->request->get('day'),
                    'name' => $this->request->get('name'),
                ]
            );
            if ($holiday->save()) {
                $this->response->redirect('holiday/index');
            }
        }
    }

    public function destroyAction()
    {
        if ($this->request->isPost())
        {
            $holiday_validate = Holiday::findFirst([
                'conditions' => 'date = :date:',
                'bind'=> [
                    'date' => $this->request->get('date')
                ]
            ]);
            $holiday_validate->delete();
        }
    }

}