<?php
declare(strict_types=1);


use Phalcon\Paginator\Adapter\NativeArray;


class IndexController extends ControllerBase
{
    public function indexAction()
    {
        if ($this->session->get('is_login')){
                $login_user_session = $this->session->get('is_login');
                $login_user = Users::findFirst([
                    'conditions' => 'id = :id:',
                    'bind' => [
                        'id' => $login_user_session['id']
                    ]
                ]);

                $users = Users::find(['conditions' => 'is_active = :is_active:',
                    'bind' => ['is_active' => true
                    ]]);

                $all_users = Users::getAllUsersFirstAuthUser($login_user,$users);

                $month = Times::getMonthArray();
                $y_arr = Times::getYearsArray();

                $current_date = new DateTime();

                if ($this->request->get())
                {
                    $calendar = Times::Calendar($this->request->get('month'),$this->request->get('year'));
                    $work_days = Times::workDays($this->request->get('month'),$this->request->get('year'));
                    $late = Times::latest($login_user->id,$this->request->get('month'),$this->request->get('year'));
                    $allTime = Times::totalTime($login_user->id,$this->request->get('month'),$this->request->get('year'));
                    $current_month = new DateTime();
                    $current_month->setTimezone(new DateTimeZone('Asia/Bishkek'));
                    $current_month->setDate((int)$this->request->get('year'),(int)$this->request->get('month'),1);
                }else{
                    $calendar = Times::Calendar();
                    $work_days = Times::workDays();
                    $late = Times::latest($login_user->id);
                    $allTime = Times::totalTime($login_user->id,$this->request->get('month'),$this->request->get('year'));
                    $current_month = new DateTime();
                    $current_month->setTimezone(new DateTimeZone('Asia/Bishkek'));
                }

                $assigned_proc =  number_format($allTime / $work_days *100,2);

                $this->view->setVar('days',$calendar);
                $this->view->setVar('users',$all_users);
                $this->view->setVar('login_user',$login_user);
                $this->view->setVar('time_nov',$current_date);
                $this->view->setVar('all_time',$allTime);
                $this->view->setVar('late',$late);
                $this->view->setVar('work_days',$work_days);
                $this->view->setVar('assigned_proc',$assigned_proc);
                $this->view->setVar('month_select',$month);
                $this->view->setVar('current_month',$current_month);
                $this->view->setVar('current_year',$y_arr);
                $this->view->setVar('tests',$all_users);
        }
    }
}

