
<div class="container">
    <div hidden id="add-holiday-modal">
        <div id="open-modal" class="modal" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 id="holiday-date-title" class="modal-title">Add Holiday</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ url('holiday/store') }}">
                            <div class="mb-3">
                                <label for="name" class="form-label">Holiday Name</label>
                                <input name="name" type="text" class="form-control" id="name" aria-describedby="name">
                                <input hidden name="year" type="text" class="form-control" id="year" aria-describedby="year">
                                <input hidden name="month" type="text" class="form-control" id="month" aria-describedby="month">
                                <input hidden name="day" type="text" class="form-control" id="day" aria-describedby="day">
                                <input hidden name="date" type="text" class="form-control" id="date" aria-describedby="date">
                            </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="modal-close" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h1 style="text-align: center; margin: 25px;">Holiday Calendar {{ current_date.format('Y') }}</h1>
    <div class="row">
    {% for month in months  %}
        <div   class="col-3">
            <h3 style="text-align: center">{{  month.format('M') }}</h3>
            <div class="card">
                <div class="card-body" style="width: 300px;">
                    <table>
                        <thead>
                        <tr>
                            {% for week in weeks_day  %}

                            <th {% if(week === 'Sat' or week == 'Sun') %} style="background-color: lightblue;"{% endif %} style="width: 30px;"> {{ week }}</th>
                            {% endfor %}
                        </tr>
                        </thead>
                            <tbody>
                            <tr>
                                {% for week in weeks_day  %}
                                    <td {% if(week === 'Sat' or week == 'Sun') %} style="background-color: lightblue;"{% endif %}">
                                    {% for day in days  %}
                                        {% if(day.format('D') === week and day.format('m') == month.format('m'))  %}
                                         <div {% if(holiday_validate !== null) %}
                                                {% if(holiday_validate.findHoliday(day.format('Y-m-d'))) %}
                                                 style="cursor: pointer; background-color: red" data-holiday="true"
                                                 {% else %}
                                                 style="cursor: pointer;"
                                                 {% endif %}
                                              {% endif %} data-year="{{ day.format('Y') }}" data-month="{{ day.format('m') }}"  data-day="{{ day.format('d') }}"  data-date="{{ day.format('Y-m-d') }}" class="holiday-day">
                                                   {{ day.format('d') }}
                                               </div>
                                        {% endif %}
                                    {% endfor %}
                                    </td>
                                {% endfor %}
                            </tr>
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    {% endfor %}
    </div>
    <div style="text-align: center; margin: 50px;" id="holidays-block">
        <div class="row">
            {% for holiday in all_holidays  %}
                <div class="col-3">
                    {{ holiday.date }} - {{ holiday.name }}
                </div>
            {% endfor %}
        </div>
    </div>
</div>
