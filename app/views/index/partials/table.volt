<table style="width: 3000px;" class="table  table-bordered table-sm">
    <thead>
    <tr>
        <th style="background-color: beige; cursor: pointer" id="test" scope="col">Hide/Show</th>
        {% for user in users %}
            {% if (session.get('is_login')['is_admin']) %}
                <th  style="background-color: beige;" scope="row"><a style=" color: black" href="{{ url(user.getUpdateUrl(user.id)) }}">{{ user.name  }}</a></th>
            {% else %}
                <th style="background-color: beige;" scope="row">{{ user.name  }}</th>
            {% endif %}
        {% endfor %}
    </tr>
    </thead>
    <tbody id="test-hide">
    {% for day in days %}
        <tr>
            <th {% if(time_nov.format('Y-m-d') !== day.format('Y-m-d'))%} class="to-hide" hidden  {% endif %}style="background-color: beige;" scope="row">{{ day.format('d D')  }}</th>
            {% for user in users %}
                <td  {% if (day.format('D') != 'Sat' and day.format('D') != 'Sun') %}
                    style="background-color: lightyellow;"
                {% else %}
                    style="background-color: lemonchiffon"
                {% endif %}
                {% if(time_nov.format('Y-m-d') !== day.format('Y-m-d')) %}
                    hidden
                    class="to-hide"
                {% endif %}>
                    <input type="checkbox" aria-label="Checkbox for following text input"><br>
                    {% if (day.format('D') != 'Sat' and day.format('D') != 'Sun') %}
                        <p></p><br>
                        <div id="time-block-{{ user.id }}-{{ day.format('d') }}">
                            {% for time in user.times %}
                                {% if(time.date === day.format('Y-m-d'))  %}
                                    <div class="row row-cols-2 stop-{{ user.id }}-{{ day.format('d') }}">
                                    <div class="col">
                                        {% if(session.get('is_login')['is_admin'])  %}
                                            <input  data-day="{{ day.format('d') }}" data-edit-start-date="{{ time.date }}" data-start-user-id="{{ user.id }}" data-start-time-id="{{ time.id }}" class="edit-start_time" type="text" style="width: 60px;" value="{{ time.start_time }}">
                                        {% else %}
                                            {{ time.start_time }}
                                        {% endif %}
                                    </div>
                                    {% if(time.end_time) %}
                                        <div class="col">
                                            {% if(session.get('is_login')['is_admin'])  %}
                                                <input data-day="{{ day.format('d') }}" data-edit-stop-date="{{ time.date }}" data-stop-user-id="{{ user.id }}" data-stop-time-id="{{ time.id }}" class="edit-stop_time" type="text" style="width: 60px;" value="{{ time.end_time }}">
                                            {% else %}
                                                {{ time.end_time }}
                                            {% endif %}
                                        </div>
                                    {% endif %}
                                {% endif %}
                                </div>
                            {% endfor %}
                        </div>
                        <div class="row row-cols-2 ">
                            <div class="col">
                                Total:
                            </div>
                            {% if (user.times.count() !== 0) %}
                                {% if(time_nov.format('d D') >= day.format('d D')) %}
                                    <div class="col total-time-{{ user.id }}-{{ day.format('d') }}">
                                        {% if (time.totalDayTime(user.id,day.format('Y-m-d')) !== '0:0') %}
                                            {{ time.totalDayTime(user.id,day.format('Y-m-d')) }}
                                        {% endif %}
                                    </div>
                                {% endif %}
                            {% else %}
                                <div  class="col total-time-{{  user.id }}-{{ day.format('d') }}">
                                </div>
                            {% endif %}
                        </div>
                        {% if (session.get('is_login')['is_admin']) %}
                            {% if(user.buttonState(user.id)) %}
                                <button  id="start-time-{{ user.id }}-{{ day.format('d') }}"  data-day="{{ day.format('Y-m-d') }}" data-day-id="{{ day.format('d') }}" data-user-id="{{ user.id }}" type="button" class="btn btn-secondary btn-sm start-time">Start</button>
                                <button style="display: none;" id="stop-time-{{ user.id }}-{{ day.format('d') }}"  data-day="{{ day.format('Y-m-d') }}" data-day-id="{{ day.format('d') }}" data-user-id="{{ user.id }}" type="button" class="btn btn-secondary btn-sm stop-time">Stop</button>
                            {% else %}
                                <button  id="stop-time-{{ user.id }}-{{ day.format('d') }}"  data-day="{{ day.format('Y-m-d') }}" data-day-id="{{ day.format('d') }}" data-user-id="{{ user.id }}" type="button" class="btn btn-secondary btn-sm stop-time">Stop</button>
                                <button  style="display: none;" id="start-time-{{ user.id }}-{{ day.format('d') }}"  data-day="{{ day.format('Y-m-d') }}" data-day-id="{{ day.format('d') }}" data-user-id="{{ user.id }}" type="button" class="btn btn-secondary btn-sm start-time">Start</button>
                            {% endif %}
                        {% else %}
                            {% if(time_nov.format('Y-m-d') == day.format('Y-m-d') and session.get('is_login')['id'] == user.id) %}
                                {% if(user.buttonState(user.id)) %}
                                    <button  id="start-time-{{ user.id }}-{{ day.format('d') }}"  data-day="{{ day.format('Y-m-d') }}" data-day-id="{{ day.format('d') }}" data-user-id="{{ user.id }}" type="button" class="btn btn-secondary btn-sm start-time">Start</button>
                                    <button style="display: none;" id="stop-time-{{ user.id }}-{{ day.format('d') }}"  data-day="{{ day.format('Y-m-d') }}" data-day-id="{{ day.format('d') }}" data-user-id="{{ user.id }}" type="button" class="btn btn-secondary btn-sm stop-time">Stop</button>
                                {% else %}
                                    <button  id="stop-time-{{ user.id }}-{{ day.format('d') }}"  data-day="{{ day.format('Y-m-d') }}" data-day-id="{{ day.format('d') }}" data-user-id="{{ user.id }}" type="button" class="btn btn-secondary btn-sm stop-time">Stop</button>
                                    <button  style="display: none;" id="start-time-{{ user.id }}-{{ day.format('d') }}"  data-day="{{ day.format('Y-m-d') }}" data-day-id="{{ day.format('d') }}" data-user-id="{{ user.id }}" type="button" class="btn btn-secondary btn-sm start-time">Start</button>
                                {% endif %}
                            {% endif %}
                        {% endif %}
                        <br>
                    {% endif %}
                </td>
            {% endfor %}
        </tr>
    {% endfor %}
</table>
