{% if(session.get('is_login')) %}
<div class="container">
{% set isNameValidClass = form.messages('name') ? 'form-control is-invalid' : 'form-control' %}
{% set isLoginValidClass = form.messages('login') ? 'form-control is-invalid' : 'form-control' %}
{% set isEmailValidClass = form.messages('email') ? 'form-control is-invalid' : 'form-control' %}
{% set isETermsValidClass = form.messages('terms') ? 'form-check-input is-invalid' : 'form-check-input' %}

<h1 class="mt-3">Update User</h1>

{{ flash.output() }}

<form method="post">
    <div class="form-group row">
        {{ form.label('name', ['class': 'col-sm-2 col-form-label']) }}
        <div class="col-sm-10">
            {{ form.render('name', ['class': isNameValidClass, 'placeholder': 'Name']) }}
            <div class="invalid-feedback">
                {{ form.messages('name') }}
            </div>
        </div>
    </div>

    <div class="form-group row">
        {{ form.label('login', ['class': 'col-sm-2 col-form-label']) }}
        <div class="col-sm-10">
            {{ form.render('login', ['class': isNameValidClass, 'placeholder': 'Login']) }}
            <div class="invalid-feedback">
                {{ form.messages('login') }}
            </div>
        </div>
    </div>

    <div class="form-group row">
        {{ form.label('email', ['class': 'col-sm-2 col-form-label']) }}
        <div class="col-sm-10">
            {{ form.render('email', ['class': isEmailValidClass, 'placeholder': 'Email']) }}
            <div class="invalid-feedback">
                {{ form.messages('email') }}
            </div>
        </div>
    </div>
    <div class="form-group row mt-3">
        <label for="is_active" class="col-sm-2 col-form-label">Active</label>
        <div class="col-sm-10">
            {{ form.render('is_active', ['class': 'form-control']) }}
        </div>
    </div>
    <div class="form-group row mt-3">
        <label for="is_active" class="col-sm-2 col-form-label">Active</label>
        <div class="col-sm-10">
            {{ form.render('is_admin', ['class': 'form-control']) }}
        </div>
    </div>
    {{ submit_button("Save", "class": "btn btn-big btn-success") }}
</form>
<hr>
</div>
{% else %}
    <p style="text-align: center; margin-top: 40px; font-size: 150px;">{{ link_to('session/login',  'class': 'nav-link', 'LOGIN') }}</p>
{% endif %}